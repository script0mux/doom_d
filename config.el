;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;;     ____  ____  ____  __  ___   ________  ______   ___________
;;    / __ \/ __ \/ __ \/  |/  /  / ____/  |/  /   | / ____/ ___/
;;   / / / / / / / / / / /|_/ /  / __/ / /|_/ / /| |/ /    \__ \
;;  / /_/ / /_/ / /_/ / /  / /  / /___/ /  / / ___ / /___ ___/ /
;; /_____/\____/\____/_/  /_/  /_____/_/  /_/_/  |_\____//____/
;;                     _____             __
;;   _________  ____  / __(_)___ _ ___  / /
;;  / ___/ __ \/ __ \/ /_/ / __ `// _ \/ /
;; / /__/ /_/ / / / / __/ / /_/ //  __/ /
;; \___/\____/_/ /_/_/ /_/\__, (_)___/_/
;;                       /____/
;;
;; File: $DOOMDIR/config.el
;; Owner: Andreas Ramalho Spyridakis <paradymns@protonmail.ch>
;; Date: 2021/07/15
;; Description: file for the user's private configuration of the Emacs framework
;; Doom Emacs.

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;;; Doom Emacs native configurations

;;; User information configurations
;; > Comments
;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
;; > Configuration code
(setq user-full-name "Andreas Ramalho Spyridakis"
      user-mail-address "paradymns@protonmail.ch")

;;; Dashboard configurations
;; > Package/Module description
;; "This module adds a minimalistic, Atom-inspired dashboard to Emacs.".
;; > Comments
;; > Configuration code
;; To use a custom image as my banner, change the `fancy-splash-image':
(setq
 fancy-splash-image "~/.zz_repo.d/imagens/icones/10_icone-Blackhole001.png")

;;; Font/Faces configurations
;; > Comments
;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
;; > Configuration code
(setq doom-font (font-spec :family "JetBrainsMono Nerd Font Mono" :size 14)
      ;;doom-variable-pitch-font (font-spec :family "mononoki")
      ;;doom-unicode-font (font-spec :family "mononoki")
      ;;doom-big-font (font-spec :family "mononoki" :size 19)
      )

;;; Line numbers configurations
;; > Comments
;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;; > Configuration code:
(setq display-line-numbers-type t)

;;; Theme configurations
;; > Comments
;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default: `doom-one'.
;; > Configuration code
(setq doom-theme 'doom-gruvbox)

;;; === Scroll configurations ===
;; --- Description ---
;; --- Comments ---
;; --- Configuration code ---
;; Set the scroll margin to n lines:
(setq scroll-margin 10) ; The cursor will scroll (bottom or top) if there is n lines to get to the margin.

;;; Doom Emacs packages/modules configurations

;;; ace-window configurations
;; > Package/Module description
;; "GNU Emacs package for selecting a window to switch to."
;; > Comments
;; "The first character of the buffers changes to a highlighted, user-selectable
;; character.
;; Pros: the content of the buffers are always visible.
;; Cons: The display characters are small and difficult to see (see below for a
;; way to enlarge them)."
;; > Configuration code
;; This changes the ace-window display to show a white letter with a red
;; background. The box attribute adds some padding.
(custom-set-faces!
  '(aw-leading-char-face
    :foreground "white" :background "red"
    :weight bold :height 2.5 :box (:line-width 10 :color "red"))
  )

;;; Evil mode configurations
;; > Package/Module description
;; "This holy module brings the vim experience to Emacs."
;; > Comments
;; > Configuration code
(after! evil
  (setq evil-ex-search-case (quote sensitive)
        evil-search-wrap nil
        )
  )

;;; Org-mode configurations
;; > Package/Module description
;; "Your life in plain text
;;
;; A GNU Emacs major mode for convenient plain text markup — and much more.
;;
;; Org mode is for keeping notes, maintaining to-do lists, planning projects,
;; authoring documents, computational notebooks, literate programming and more —
;; in a fast and effective plain text system.".
;; > Comments
;; If you use `org' and don't want your org files in the default
;; location below, change `org-directory'. It must be set before org loads!
;; > Configuration code
(setq org-directory "~/org/")
(after! org ;; Load the following code after the org-mode package is loaded.
  (setq
   ;; Overwrite Doom Emacs default `TODO' keywords:
   org-todo-keywords
   '((sequence
      "INBOX(i)"
      "TODO(t)"
      "SCHEDULED(s)"
      "WAITING(w)"
      "SOMEDAY(o)"
      "|"
      "DONE(d)"
      "CANCELED(c)"
      "ABSENT(a)"))
   org-todo-keywords-for-agenda
   '((sequence
      "INBOX(i)"
      "TODO(t)"
      "SCHEDULED(s)"
      "WAITING(w)"
      "SOMEDAY(o)"
      "|"
      "DONE(d)"
      "CANCELED(c)"
      "ABSENT(a)"))
   ;; Set the agenda file(s):
   org-agenda-files
   '("~/.zz_repo/arquivos/Aa/Agenda")
   ;; TODO items closing notes configurations:
   org-log-done 'time ; Insert line `CLOSED: [timestamp]' under the TODO item
                      ; when it's changed to a DONE state.
   ;;org-log-done 'note ; To record a note along with the timestamp.
   ))

;;; rgb module configurations
;; > Package/Module description
;; "Highlights color hex values and names with the color itself, and provides
;; tools to easily modify color values or formats."
;; > Comments
;; `hl-line-mode' overrides the color highlighting of `rainbow-mode', limiting
;; the use of that plugin and on-site color changes using `kurecolor'. To
;; automatically disable it only when `rainbow-mode' is active, you can add the
;; following hook:
;; (add-hook! 'rainbow-mode-hook
;;            (hl-line-mode (if rainbow-mode -1 +1))
;;            )
;; > Configuration code
(add-hook! 'rainbow-mode-hook
  (hl-line-mode (if rainbow-mode -1 +1))
  )

;;; Treemacs configurations
;; > Package/Module description
;; "Treemacs is a file and project explorer similar to NeoTree or vim’s
;; NerdTree, but largely inspired by the Project Explorer in Eclipse. It shows
;; the file system outlines of your projects in a simple tree layout allowing
;; quick navigation and exploration, while also possessing basic file management
;; utilities".
;; > Comments
;; > Configuration code

;; === `org-brain' configurations ===
;; --- Description ---
;; --- Comments ---
;; --- Code ---
;; Configure `org-brain-path' to a directory where I want to put my `org-brain'
;; files:
;; (setq org-brain-path "~/.zz_repo/logiciarios/Org_Mode-org/dados/org-brain")
;; After loading `evil', set this option (for `evil' users):
;; (with-eval-after-load 'evil
;;   (evil-set-initial-state 'org-brain-visualize-mode 'emacs)
;;   )
;; Modify the location for the `org-id-locations-file':
;; (setq org-id-locations-file "~/.zz_repo/logiciarios/Org_Mode-org/dados/org-brain")

;;: === `org-roam' configurations ===
;;: --- Description ---
;;: A plain-text personal knowledge management system.
;;: --- Comments ---
;;: --- Configuration code ---
;;: Set `org-roam' to ignore the migration warning from version 1 to version 2:
(setq org-roam-v2-ack t)
;;: Set the `org-roam' directory:
(setq org-roam-directory "~/.zz_repo/documentos/Zz/Zettelkasten/org-roam/notas")
;;: Set the `org-roam' database location:
(setq org-roam-db-location "~/.zz_repo/documentos/Zz/Zettelkasten/org-roam/banco_de_dados/org-roam.db")
;;: Set keybinds for org-roam common commands:
(map!
 :desc "Launch an Org-roam buffer that tracks the node currently at point. This means that the content of the buffer changes as the point is moved, if necessary."
 :g "C-c n l" #'org-roam-buffer-toggle
 )
(map!
 :desc "Creates a node if it does not exist, and visits the node."
 :g "C-c n f" #'org-roam-node-find
 )
(map!
 :desc "Creates a node if it does not exist, and inserts a link to the node at point."
 :g "C-c n i" #'org-roam-node-insert
 )
;;: Setup `org-roam' to run functions on file changes to maintain cache consistency:
(org-roam-db-autosync-mode)
;;: Set `org-roam' up:
;; (org-roam-setup)

;;; === `org-ref' configurations ===
;; --- Description ---
;; Emacs package for handling bibliographic references with org documents.
;; --- Comments ---
;; --- Code ---
(setq bibtex-completion-bibliography '("~/.zz_repo/logiciarios/JabRef/dados/jabref_bibrefs_index.bib"
                                                ;;"~/Dropbox/emacs/bibliography/dei.bib"
                                                ;;"~/Dropbox/emacs/bibliography/master.bib"
                                                ;;"~/Dropbox/emacs/bibliography/archive.bib"
                                                )
        ;;bibtex-completion-library-path '("~/Dropbox/emacs/bibliography/bibtex-pdfs/")
        ;;bibtex-completion-notes-path "~/Dropbox/emacs/bibliography/notes/"
        ;;bibtex-completion-notes-template-multiple-files "* ${author-or-editor}, ${title}, ${journal}, (${year}) :${=type=}: \n\nSee [[cite:&${=key=}]]\n"

        bibtex-completion-additional-search-fields '(keywords)
        bibtex-completion-display-formats
        '((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
                (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
                (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
                (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
                (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
        bibtex-completion-pdf-open-function
        (lambda (fpath)
                (call-process "open" nil 0 nil fpath))
        )

;; === `org-drill' configurations ===
;; --- Description ---
;; Section for customizing `org-drill'
;; --- Comments ---
;; --- Code ---
;;(setq org-drill-scope '(directory)) ; Set the sources of items for drill sessions (scope).

;;; Doom Emacs keybinds configurations

;;; Keybinds configurations for rgb module
;; > Comments
;; > Configuration code
(map!
 ;; Toggle rainbow-mode:
 :n "SPC z t r" #'rainbow-mode
 )

;;; Keybinds configurations for treemacs
;; > Comments
;; > Configuration code
(map!
 ;; Open treemacs:
 :n "SPC z o t" #'treemacs
 )

;;; === Keybinds configurations for `org-ref' ===
;; --- Comments ---
;; --- Code ---
(define-key org-mode-map (kbd "C-c ]") 'org-ref-insert-link) ; For "C-c ]", "C-u C-c ]" and "C-u C-u C-c ]".

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
