;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;;     ____  ____  ____  __  ___   ________  ______   ___________
;;    / __ \/ __ \/ __ \/  |/  /  / ____/  |/  /   | / ____/ ___/
;;   / / / / / / / / / / /|_/ /  / __/ / /|_/ / /| |/ /    \__ \
;;  / /_/ / /_/ / /_/ / /  / /  / /___/ /  / / ___ / /___ ___/ /
;; /_____/\____/\____/_/  /_/  /_____/_/  /_/_/  |_\____//____/
;;                       __                               __
;;     ____  ____ ______/ /______ _____ ____  _____ ___  / /
;;    / __ \/ __ `/ ___/ //_/ __ `/ __ `/ _ \/ ___// _ \/ /
;;   / /_/ / /_/ / /__/ ,< / /_/ / /_/ /  __(__  )/  __/ /
;;  / .___/\__,_/\___/_/|_|\__,_/\__, /\___/____(_)___/_/
;; /_/                          /____/
;;
;; File: $DOOMDIR/packages.el
;; Owner: Andreas Ramalho Spyridakis <paradymns@protonmail.ch>
;; Date: 2021/07/15
;; Description: file used for managing Emacs packages not included in any of the
;; Doom Emacs' original modules.

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)

;;
;; Start of the user's lines on file `packages.el'.
;;

;; === `package!' declaration for installing the package `org-drill' ===
;; --- Description ---
;; Org-Drill is an extension of org-mode for implementing a spaced repetition
;; function to org-mode.
;; --- Comments ---
;; --- Code ---
;; Specify that the package `org-drill' will need a dependency, which is `org':
(when (package! org-drill) ; "When installing the package `org-drill'..."
  (package! org) ; "... verify if the package `org' is installed, before installing `org-drill'."
  )

;; === `package!' declaration for installing the package `org-brain' ===
;; --- Description ---
;; `org-brain' implements a variant of concept mapping in Emacs, using
;; `org-mode'. It is heavily inspired by a piece of software called The Brain,
;; and you can view an introduction to that program here.
;; --- Comments ---
;; --- Code ---
(when (package! org-brain
        ;; :disable t
        ) ; "When installing the package `org-brain'..."
  (package! org) ; "... verify if the package `org' is installed before installing `org-brain'."
  )

;;: === `package!' declaration for installing the package `org-roam' ===
;;: --- Description ---
;;: A plain-text personal knowledge management system.
;;: --- Comments ---
;;: --- Configuration code ---
(when (package! org-roam) ; "When installing the package `org-roam'..."
  (package! dash) ; "... verify if the packages cited are installed before installing `org-roam'."
  (package! f)
  (package! s)
  (package! org)
  (package! emacsql)
  (package! emacsql-sqlite)
  (package! magit-section)
  )

;;: === `package!' declaration for installing the package `auctex' ===
;;: --- Description ---
;;: "AUCTEX is an extensible package for writing and formatting TEX files in GNU
;;: Emacs. It supports many different TEX macro packages, including AMS-TEX,
;;: LATEX, Texinfo, ConTEXt, and docTEX (dtx files).
;;:
;;: AUCTEX includes preview-latex which makes LATEX a tightly integrated
;;: component of your editing workflow by visualizing selected source chunks
;;: (such as single formulas or graphics) directly as images in the source
;;: buffer."
;;: --- Comments ---
;;: --- Configuration code ---
(package! auctex)

;; ===`package!' declaration for installing the package `org-ref' ===
;; --- Description ---
;; "org-mode modules for citations, cross-references, bibliographies in org-mode
;; and useful bibtex tools to go with it."
;; --- Comments ---
;; --- Code ---
(when (package! org-ref)
  (package! org)
  )

;; === `package!' declaration for installing the package `org-drill' ===
;; --- Description ---
;; --- Comments ---
;; --- Code ---
(when (package! org-drill)
  (package! org)
  )

;;
;; End of the user's lines on file `packages.el'.
;;
